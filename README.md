
Pasos Para Hacer correr 
Lenguaje de Programación: Java
IDE Recomendado: Eclipse, IntelliJ IDEA, NetBeans
Herramienta de Construcción: Maven





Capa de Presentación:
Encargada de la interacción con el usuario.
Capa de Lógica de Negocio:
Contiene la lógica de negocio de la aplicación.
Capa de Acceso a Datos:
Gestiona la comunicación con la base de datos.

PRINCIPIOS SOLOD:
Single Responsibility Principle (SRP): La clase CitaMedica se encarga únicamente de representar la información básica de una cita médica y de realizar la reserva de la cita. No se mezclan responsabilidades adicionales como asignación o notificación.
Open/Closed Principle (OCP): La clase CitaMedica es una clase abstracta que puede ser extendida para crear diferentes tipos de citas médicas, como CitaMedicas, permitiendo que el sistema sea extendido sin modificar el código existente.
Liskov Substitution Principle (LSP): La implementación de Paciente como Notificacion permite que cualquier objeto Paciente sea usado en lugar de cualquier implementación de Notificacion, manteniendo el comportamiento esperado sin necesidad de cambios adicionales.
Interface Segregation Principle (ISP): La interfaz Notificacion define un único método notificar(), asegurando que las clases solo implementen los métodos que necesitan para su función específica de notificación.
Dependency Inversion Principle (DIP): GestorCitasMedicas depende de abstracciones como Notificacion y CitaMedica, en lugar de clases concretas. Esto facilita la extensibilidad del sistema al permitir la adición de nuevos tipos de citas médicas o métodos de notificación sin modificar el gestor de citas médicas.






Patrones de Diseño:
Adapter (Adaptador):
Utilizado para adaptar la clase paciente  a la interfaz Notificacion permitiendo que los objetos   paciente   puedan recibir y manejar notificaciones del sistema de citas médicas.
Observer (Observador):
Implementado para notificar a los pacientes cuando se asigna y completa una cita médica. El gestor de notificaciones actúa como el sujeto observado y los pacientes adaptados (pacienteAdapter) actúan como los observadores.
Factory Method (Método de Fábrica):
Empleado para crear instancias de citas médicas de manera flexible. Se definen dos fábricas concretas (CitaMedicaNormalFactory y CitaMedicaEmergenciaFactory) que implementan la interfaz CitaMedicaNormalFactory, permitiendo al sistema crear citas médicas normales y de emergencia según sea necesario.

Arquitectura en 3 capas:
Capa de presentación (presentacion):
SistemaDeCitasMedicas: Se encarga de la interacción con el usuario y la coordinación de las operaciones del sistema. Aquí se pueden crear instancias de controladores o manejadores para manejar las solicitudes del usuario.
Capa de negocio (negocio):
CitaMedica: Representa la información básica de una cita médica y define el método abstracto reservarCita().
CitaMedicaEmergencia: Implementa CitaMedica para representar una cita médica de emergencia.
CitaMedicaEmergenciaFactory: Implementa CitaMedicaFactory para crear instancias de CitaMedicaEmergencia.
CitaMedicaNormalFactory: Implementa CitaMedicaFactory para crear instancias de citas médicas normales.
GestorCitasMedicas: Gestiona la lógica de negocio relacionada con la gestión de citas médicas, como agregar citas médicas, asignar citas y completar citas.
GestorNotificaciones: Gestiona la lógica de negocio relacionada con las notificaciones a los pacientes.
Notificacion: Interfaz que define el método notificar() para enviar notificaciones a los pacientes.
Paciente: Representa un paciente y también actúa como una implementación de Notificacion.
PacienteAdapter: Adapta la clase Paciente a la interfaz Notificacion para que los pacientes puedan recibir y manejar notificaciones del sistema de citas médicas.
Capa de acceso a datos (datos):
CitasMedicasDatos: Gestiona el almacenamiento y recuperación de datos relacionados con las citas médicas.
NotificacionesDatos: Gestiona el almacenamiento y recuperación de datos relacionados con las notificaciones.

