package negocio;

public interface CitaMedicaFactory {
    CitaMedica crearCitaMedica(String nombrePaciente, String nombreDoctor, String fecha, String hora);
}