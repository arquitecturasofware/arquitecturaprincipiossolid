package negocio;
public abstract class CitaMedica {
    protected String nombrePaciente;
    protected String nombreDoctor;
    protected String fecha;
    protected String hora;

    public CitaMedica(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        this.nombrePaciente = nombrePaciente;
        this.nombreDoctor = nombreDoctor;
        this.fecha = fecha;
        this.hora = hora;
    }

    public String getNombrePaciente() {
        return nombrePaciente;
    }

    public abstract void reservarCita();
}
