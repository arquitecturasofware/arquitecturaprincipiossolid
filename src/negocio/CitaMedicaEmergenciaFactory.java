
package negocio;
public class CitaMedicaEmergenciaFactory implements CitaMedicaFactory {
    @Override
    public CitaMedicaEmergencia crearCitaMedica(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        return new CitaMedicaEmergencia(nombrePaciente, nombreDoctor, fecha, hora);
    }
}
