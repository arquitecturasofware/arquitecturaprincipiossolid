
package negocio;
public class CitaMedicaEmergencia extends CitaMedica {
    public CitaMedicaEmergencia(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        super(nombrePaciente, nombreDoctor, fecha, hora);
    }

    @Override
    public void reservarCita() {
        System.out.println("Reservando cita de emergencia para " + nombrePaciente +
                " con el doctor " + nombreDoctor +
                " el día " + fecha +
                " a las " + hora);
    }
    public String toString() {
        return "Reservado cita de emergencias para " + nombrePaciente + " con el doctor " + nombreDoctor +
               " el día " + fecha + " a las " + hora;
    }
}
