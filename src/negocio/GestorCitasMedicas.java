
package negocio;
import datos.CitasMedicasDatos;
import datos.NotificacionesDatos;
import java.util.*;

public class GestorCitasMedicas {
    private CitasMedicasDatos citasMedicasDatos = new CitasMedicasDatos();
    private NotificacionesDatos NotificacionesDatos = new NotificacionesDatos();
    private CitaMedicaFactory citaMedicaNormalFactory;
    private CitaMedicaFactory citaMedicaUrgenciaFactory;

    public GestorCitasMedicas(CitaMedicaFactory citaMedicaNormalFactory, CitaMedicaFactory citaMedicaUrgenciaFactory) {
        this.citaMedicaNormalFactory = citaMedicaNormalFactory;
        this.citaMedicaUrgenciaFactory = citaMedicaUrgenciaFactory;
    }

    public List<CitaMedica> getCitasMedicas() {
        return citasMedicasDatos.getCitasMedicas();
    }

    public void agregarCitaMedicaNormal(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        CitaMedica citaMedica = citaMedicaNormalFactory.crearCitaMedica(nombrePaciente, nombreDoctor, fecha, hora);
        citasMedicasDatos.agregarCitaMedica(citaMedica);
    }

    public void agregarCitaMedicaUrgencia(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        CitaMedica citaMedica = citaMedicaUrgenciaFactory.crearCitaMedica(nombrePaciente, nombreDoctor, fecha, hora);
        citasMedicasDatos.agregarCitaMedica(citaMedica);
    }

    public void asignarCitaMedica(CitaMedica citaMedica, Notificacion notificacion) {
        NotificacionesDatos.agregarNotificacion(notificacion);
        notificacion.notificar("Cita médica asignada para " + citaMedica.getNombrePaciente());
    }

    public void completarCitaMedica(CitaMedica citaMedica) {
        citaMedica.reservarCita();
        notificarATodos("Cita médica completada para " + citaMedica.getNombrePaciente());
    }

    private void notificarATodos(String mensaje) {
        List<Notificacion> notificaciones = NotificacionesDatos.getNotificaciones();
        for (Notificacion notificacion : notificaciones) {
            notificacion.notificar(mensaje);
        }
    }
}
