package negocio;

public class PacienteAdapter implements Notificacion {
    private Paciente paciente;

    public PacienteAdapter(Paciente paciente) {
        this.paciente = paciente;
    }

    @Override
    public void notificar(String mensaje) {
        paciente.notificar(mensaje);
    }
   
}