package negocio;
import java.util.*;
import datos.NotificacionesDatos; 

public class GestorNotificaciones {
    private NotificacionesDatos notificacionesDatos = new NotificacionesDatos();
    public void agregarNotificacion(Notificacion notificacion) {
        notificacionesDatos.agregarNotificacion(notificacion); 
    }

    public void notificarPacientes(String mensaje) {
        List<Notificacion> notificaciones = notificacionesDatos.getNotificaciones(); 
        for (Notificacion notificacion : notificaciones) {
            Paciente paciente = (Paciente) notificacion;
            notificacion.notificar(paciente.getNombre() + ": " + mensaje); 
        }
    }
    
}

