package negocio;

public class Paciente implements Notificacion {
    private String nombre;

    public Paciente(String nombre) {
        this.nombre = nombre;
    }
 
    public String getNombre() {
        return nombre;
    }
    @Override
    public void notificar(String mensaje) {
        System.out.println(nombre + ": " + mensaje);
    }
}
