

package negocio;

public class CitaMedicaNormalFactory implements CitaMedicaFactory {
    @Override
    public CitaMedica crearCitaMedica(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        return new CitaMedicas(nombrePaciente, nombreDoctor, fecha, hora);
    }
}