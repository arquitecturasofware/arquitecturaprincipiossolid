package negocio;

class CitaMedicas extends CitaMedica {
    public CitaMedicas(String nombrePaciente, String nombreDoctor, String fecha, String hora) {
        super(nombrePaciente, nombreDoctor, fecha, hora);
    }

    @Override
    public void reservarCita() {
        System.out.println("Reservando cita médica para " + nombrePaciente +
                " con el doctor " + nombreDoctor +
                " el día " + fecha +
                " a las " + hora);
    }
    public String toString() {
        return "Reservando cita medica Normal " + nombrePaciente + " con el doctor " + nombreDoctor +
               " el día " + fecha + " a las " + hora;
    }
}