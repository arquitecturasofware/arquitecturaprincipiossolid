package datos;

import java.util.ArrayList;
import java.util.List;

import negocio.Notificacion;


public class NotificacionesDatos {
 private List<Notificacion> notificaciones = new ArrayList<>();

 public void agregarNotificacion(Notificacion notificacion) {
        notificaciones.add(notificacion);
    }

 public List<Notificacion> getNotificaciones() {
        return notificaciones;
    }




}
