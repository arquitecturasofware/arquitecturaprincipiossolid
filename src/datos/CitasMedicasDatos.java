package datos;

import java.util.ArrayList;
import java.util.List;

import negocio.CitaMedica;

public class CitasMedicasDatos {
    
    private List<CitaMedica> citasMedicas = new ArrayList<>();

    public void agregarCitaMedica(CitaMedica citaMedica) {
        citasMedicas.add(citaMedica);
    }

    public List<CitaMedica> getCitasMedicas() {
        return citasMedicas;
    }

}
