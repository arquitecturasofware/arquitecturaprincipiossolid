package presentacion;

import java.util.*;

import negocio.*;

public class SistemaDeCitasMedicas {
    public static void main(String[] args) {
        CitaMedicaFactory citaMedicaNormalFactory = new CitaMedicaNormalFactory();
        CitaMedicaFactory citaMedicaEmergenciaFactory = new CitaMedicaEmergenciaFactory();
        GestorNotificaciones gestorNotificaciones = new GestorNotificaciones();

        GestorCitasMedicas gestorCitasMedicas = new GestorCitasMedicas(citaMedicaNormalFactory, citaMedicaEmergenciaFactory);

        gestorCitasMedicas.agregarCitaMedicaNormal("Juan gonzales ", "Doctor Luis martinez", "2024-05-10", "09:00");
        gestorCitasMedicas.agregarCitaMedicaUrgencia("María perez", "Doctora andrea lopez", "2024-05-11", "15:30");
        gestorCitasMedicas.agregarCitaMedicaNormal("Carlos gomez", "Doctor albert camacho ", "2024-05-12", "11:00");
        gestorCitasMedicas.agregarCitaMedicaUrgencia("Ana ramirez", "Doctor nicolas fuentes", "2024-05-13", "14:00");

        System.out.println("Todas las citas médicas:");
        List<CitaMedica> citasMedicas = gestorCitasMedicas.getCitasMedicas();
        for (CitaMedica citaMedica : citasMedicas) {
            System.out.println(citaMedica);
        }

        Paciente paciente = new Paciente("Juan");
        Notificacion adaptador = new PacienteAdapter(paciente);
        
        gestorNotificaciones.agregarNotificacion(paciente);
        gestorNotificaciones.notificarPacientes("Su cita médica está programada para mañana a las 10:00.");

        CitaMedica citaMedica = gestorCitasMedicas.getCitasMedicas().get(0);
        gestorCitasMedicas.asignarCitaMedica(citaMedica, adaptador);
        gestorCitasMedicas.completarCitaMedica(citaMedica);
    }

}
